版本信息记录表
lodas_hoya_Install_20211008.zip
解决overflow问题

lodas_hoya_offline_Install_20210507.exe
增加当前点的打印，用于提高出报告效率

-----------------------------20210326（未发布）
1 save file时使用filter保存
-----------------------------20210301（未验证，未发布）
1 AF增加CANCEL功能

-----------------------------20210128（发布给客户）
Lodas_hoya_Version_update_20210127.zip
1 概率计算基本功能
2 显示map崩溃问题解决 （void Map2::ResetControl() ScrollBar^  值 out of range）

-----------------------------20210127（暂未发布）
Lodas_hoya_Version_update_20210127.zip
2 显示map崩溃问题解决 （void Map2::ResetControl() ScrollBar^  值 out of range）

-----------------------------20210125 (已经发送给客户)
Lodas_hoya_Version_update_20210125.zip
1 增加compare新算法，去掉grid
2 两个算法并存，算法1进行了更新

-----------------------------20210118（USING）
解决online offline merge结果不一致问题

-----------------------------20201222(USING)
Lodas_Version_update_20201222
compare文档内容压缩

-----------------------------20201211
Lodas_Version_update_20201211.zip
解决compare功能失效问题

-----------------------------20201203
Lodas_Version_update_20201202.zip
Lodas_HOYA_offline_Install-20201202.zip
客户的需求修改版本发布
两个版本，V1 new merge，无grid merge
V2 merge先grid后，加一个similar参数

-----------------------------20201117
Lodas_hoya_Install-20201117-1
compare功能交付

-----------------------------20201112
Lodas_hoya_Install-20201112-1.zip
PDF正常版本

-----------------------------20201030
Lodas_hoya_Install-20201030-1
给hoya提供的第一个交付版本

